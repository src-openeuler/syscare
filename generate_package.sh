#!/bin/bash -e

readonly SPEC_FILE="$(find . -name '*.spec' | head -n 1)"

readonly REPO_NAME="$(basename ${SPEC_FILE} | sed 's/.spec//')"
readonly REPO_URL="https://gitee.com/openeuler/${REPO_NAME}"
readonly REPO_BRANCH="$(git branch --show-current | sed 's/-LTS.*//')"
readonly REPO_VERSION="$(grep Version ${SPEC_FILE} | head -n 1 | awk -F ' ' '{print $NF}')"

readonly PKG_NAME="${REPO_NAME}-${REPO_VERSION}"
readonly PKG_DIR="$(realpath ./${PKG_NAME})"

echo "Preparing..."
rm -rf ./${REPO_NAME} ./${PKG_NAME}
rm -f ./*.tar.gz

echo "--------------------------"
echo "Name:   ${REPO_NAME}"
echo "Branch: ${REPO_BRANCH}"
echo "--------------------------"

echo "Cloning source code..."
git clone ${REPO_URL} -b ${REPO_BRANCH} ${PKG_NAME}

echo "Vendoring dependencies..."
pushd ${PKG_DIR} > /dev/null

cargo vendor --quiet --respect-source-config --sync Cargo.toml

mkdir -p .cargo
cat << EOF > .cargo/config.toml
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

popd > /dev/null

echo "Compressing package..."
tar -czf ./${PKG_NAME}.tar.gz ${PKG_NAME}

echo "Cleaning up..."
rm -rf ${PKG_DIR}

echo "Done"
