#!/bin/bash -e

readonly SPEC_FILE="$(find . -name '*.spec' | head -n 1)"

readonly REPO_NAME="$(basename ${SPEC_FILE} | sed 's/.spec//')"
readonly REPO_URL="https://gitee.com/openeuler/${REPO_NAME}"
readonly REPO_BRANCH="$(git branch --show-current | sed 's/-LTS.*//')"
readonly REPO_VERSION="$(grep Version ${SPEC_FILE} | head -n 1 | awk -F ' ' '{print $NF}')"

readonly PKG_NAME="${REPO_NAME}-${REPO_VERSION}"
readonly PKG_DIR="$(realpath ./${PKG_NAME})"

readonly PATCH_DIR="$(pwd)"

echo "Preparing..."
rm -rf ${PKG_DIR}
rm -f ./*.patch

tar -xf ./${PKG_NAME}.tar.gz

pushd ${PKG_DIR} > /dev/null
readonly REPO_BASELINE="$(git rev-parse --short HEAD)"
popd > /dev/null

echo "------------------------------"
echo "Name:     ${REPO_NAME}"
echo "Branch:   ${REPO_BRANCH}"
echo "Baseline: ${REPO_BASELINE}"
echo "------------------------------"

echo "Syncing with remote..."
pushd ${PKG_DIR} > /dev/null
git fetch origin
popd > /dev/null

echo "Generating patches..."
# format patches
pushd ${PKG_DIR} > /dev/null
git checkout -q origin/${REPO_BRANCH}
git format-patch -qN -o ${PATCH_DIR} ${REPO_BASELINE}
popd > /dev/null

# print patch list
patch_list="$(find . -maxdepth 1 -name "*.patch" | sort)"
for patch_file in ${patch_list}; do
    patch_name="$(basename ${patch_file})"
    patch_id="$(echo ${patch_name} | awk -F '-' '{print $1}')"
    echo "Patch${patch_id}: ${patch_name}"
done

echo "Cleaning up..."
rm -rf ${PKG_DIR}

echo "Done"
