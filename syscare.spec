%define build_version %{version}-%{release}

############################################
############ Package syscare ###############
############################################
Name:          syscare
Version:       1.2.2
Release:       4
Summary:       System hot-fix service
License:       MulanPSL-2.0 and GPL-2.0-only
URL:           https://gitee.com/openeuler/syscare
Source0:       %{name}-%{version}.tar.gz

BuildRequires: cmake >= 3.14 make
BuildRequires: rust >= 1.51 cargo >= 1.51
BuildRequires: gcc gcc-c++
Requires:      coreutils systemd
Requires:      kpatch-runtime

Excludearch:   loongarch64

Patch0001: 0001-fix-kernel-patch-ACCEPTED-change-to-DEACTIVED-after-.patch
Patch0002: 0002-fix-some-clean-code-problem.patch
Patch0003: 0003-syscared-fix-active-accepted-patch-failure-issue.patch
Patch0004: 0004-upatch-helper-fix-object-upatch-id-duplicated-issue.patch
Patch0005: 0005-syscare-build-update-README.md.patch
Patch0006: 0006-syscared-support-saving-restoring-patch-status-by-op.patch
Patch0007: 0007-project-update-Cargo.lock.patch
Patch0008: 0008-all-remove-signal-handler.patch
Patch0009: 0009-project-update-Cargo.lock.patch
Patch0010: 0010-syscare-remove-working-directory-check.patch
Patch0011: 0011-all-fix-cargo-clippy-warnings.patch

############### Description ################
%description
SysCare is a system-level hot-fix service that provides security patches and system error hot-fixes for the operating system.
The host can fix the system problem without rebooting.

############## BuildPreparare ##############
%prep
%autosetup -p1

################## Build ###################
%build
mkdir -p build
cd build

cmake \
    -DENABLE_ASAN=0 \
    -DENABLE_GCOV=0 \
    -DBUILD_VERSION=%{build_version} \
    -DCMAKE_INSTALL_PREFIX=/usr \
    ..

make

################# Install ##################
%install
cd build
%make_install

############### PostInstall ################
%post
mkdir -p /usr/lib/syscare/patches

systemctl daemon-reload
systemctl enable syscare
systemctl start syscare

############### PreUninstall ###############
%preun
if [ "$1" -eq 0 ]; then
    systemctl daemon-reload
    systemctl stop syscare
    systemctl disable syscare
fi

############## PostUninstall ###############
%postun
if [ "$1" -eq 0 ] || { [ -n "$2" ] && [ "$2" -eq 0 ]; }; then
    # Remove patch directory
    rm -rf /usr/lib/syscare

    # Remove log directory
    rm -f /var/log/syscare/syscared_r*.log
    rm -f /var/log/syscare/syscared_r*.log.gz
    if [ -z "$(ls -A /var/log/syscare)" ]; then
        rm -rf /var/log/syscare
    fi

    # Remove run directory
    rm -f /var/run/syscare/patch_op.lock
    rm -f /var/run/syscare/syscared.*
    if [ -z "$(ls -A /var/run/syscare)" ]; then
        rm -rf /var/run/syscare
    fi
fi

################## Files ###################
%files
%defattr(-,root,root,0555)
%dir /usr/libexec/syscare
%attr(0555,root,root) /usr/bin/syscare
%attr(0550,root,root) /usr/bin/syscared
%attr(0550,root,root) /usr/libexec/syscare/upatch-manage
%attr(0550,root,root) /usr/lib/systemd/system/syscare.service

############################################
########## Package syscare-build ###########
############################################
%package build
Summary: Syscare build tools.
BuildRequires: elfutils-libelf-devel
Requires: coreutils
Requires: patch
Requires: kpatch
Requires: tar gzip
Requires: rpm rpm-build

############### Description ################
%description build
Syscare patch building toolset.

################## Files ###################
%files build
%defattr(-,root,root,0555)
%dir /usr/libexec/syscare
%attr(555,root,root) /usr/libexec/syscare/syscare-build
%attr(555,root,root) /usr/libexec/syscare/upatch-build
%attr(555,root,root) /usr/libexec/syscare/upatch-diff
%attr(555,root,root) /usr/libexec/syscare/upatch-helper
%attr(555,root,root) /usr/libexec/syscare/upatch-cc
%attr(555,root,root) /usr/libexec/syscare/upatch-c++

############################################
################ Change log ################
############################################
%changelog
* Thu Feb 20 2025 renoseven<dev@renoseven.net> - 1.2.2-4
- all: fix rust 1.84 compile failure
- syscare: fix cannot find working directory
- syscare-build: fix set signal handler failure
- syscared: fix active accepted patch failure

* Mon Nov 11 2024 renoseven<dev@renoseven.net> - 1.2.2-3
- syscared: support saving & restoring patch status by operation order

* Wed Sep 18 2024 renoseven<dev@renoseven.net> - 1.2.2-2
- syscared: fix kernel patch apply failure issue
- upatch-helper: fix object upatch id duplicated issue

* Fri Aug 16 2024 renoseven<dev@renoseven.net> - 1.2.2-1
- Release version 1.2.2-1.
